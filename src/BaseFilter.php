<?php

namespace Drupal\urlicon;

/**
 * The abstract base filter.
 */
abstract class BaseFilter implements FilterInterface {

  const REGEX_AHREF_URL = '/<a.+?href=\"([http|https|ftp|telnet|news|mms]{0,}:\/\/.+?)\"[^>]*>(.+?)<\/a>/s';

  /**
   * {@inheritdoc}
   */
  public static function filter(string $string):string {
    return preg_replace_callback(
      self::REGEX_AHREF_URL,
      'static::callback',
      $string
    );
  }

  /**
   * The filter callback.
   *
   * @param array $matches
   *   The regex matches.
   *
   * @return string
   *   The modified string.
   */
  protected static function callback(array $matches):string {
    return (string) $matches[0];
  }

}
