<?php

namespace Drupal\urlicon;

use Drupal\Component\Utility\Html;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Template\Attribute;
use Drupal\file\Entity\File;

/**
 * The favicon filter.
 */
class FaviconFilter extends BaseFilter {

  // Possible content-types.
  const CONTENT_TYPES = [
    // Suggested by IANA.
    'application/ico',
    'application/octet-stream',
    'image/vnd.microsoft.icon',
    'image/ico',
    'image/icon',
    'image/x-icon',
    'text/ico',
    'text/plain',
    // Suggested by W3C.
    'image/gif',
    'image/png',
  ];

  // Maximum content size.
  const MAX_BYTES = 1048576;

  /**
   * {@inheritdoc}
   */
  protected static function callback(array $matches):string {
    $hostname = parse_url($matches[1], PHP_URL_HOST);
    $host = Html::escape(str_replace('.', '-', $hostname));

    /** @var \Drupal\Core\File\FileSystem $file_system */
    $file_system = \Drupal::service('file_system');
    if (!file_exists('public://urlicon/')) {
      $file_system->mkdir('public://urlicon/');
    }

    /** @var \Drupal\file\FileRepository $file_service */
    $file_service = \Drupal::service('file.repository');

    // The default favicon.
    $favicon = \Drupal::service('extension.list.module')->getPath('urlicon') . '/assets/favicon.ico';
    // Check for local copy of the favicon.
    if ($file = $file_service->loadByUri('public://urlicon/' . $host . '.ico')) {
      $favicon = $file->getFileUri();
    }
    // Fetch and create a local copy of the favicon.
    else {
      if ($file = self::fetchFavicon($hostname)) {
        $favicon = $file->getFileUri();
      }
    }

    $element = [
      '#theme' => 'urlicon',
      '#text' => $matches[2],
      '#favicon' => \Drupal::service('file_url_generator')
        ->generateString($favicon),
      '#path' => $matches[1],
      '#attributes' => new Attribute([
        'alt' => '',
        'title' => t('favicon'),
        'class' => [
          'urlicon',
          'urlicon-' . $host,
        ],
      ]),
    ];
    $link = \Drupal::service('renderer')
      ->renderPlain($element);
    return trim($link);
  }

  /**
   * Fetch a favicon from a domain.
   *
   * @param string $host
   *   The hostname.
   *
   * @return \Drupal\file\Entity\File|false
   *   The favicon file or false.
   */
  private static function fetchFavicon(string $host):File|FALSE {
    /** @var \GuzzleHttp\Client $http_client */
    $http_client = \Drupal::service('http_client');
    /** @var \Drupal\file\FileRepository $file_service */
    $file_service = \Drupal::service('file.repository');

    // Check for favicon in link tags.
    $data = $http_client->get($host)->getBody()->getContents();
    if (preg_match('/<link[^>]+rel="(?:shortcut )?icon"[^>]+?href="([^"]+?)"/si', $data, $icons)) {
      // Get favicon from absolute path.
      if (strpos($icons[1], '://')) {
        $url = $icons[1];
      }
      // Get favicon from relative path.
      else {
        $url = $host . '/' . trim($icons[1], '/');
      }
    }

    // Get favicon from webroot.
    if (empty($url)) {
      $url = $host . '/favicon.ico';
    }

    try {
      $response = $http_client->get($url);

      $header = $response->getHeader('content-type')[0];
      if (!in_array($header, self::CONTENT_TYPES)) {
        throw new \Exception('Unknown content-type: ' . $header, 100);
      }

      if ($response->getBody()->getSize() > self::MAX_BYTES) {
        throw new \Exception('Response body is larger than ' . self::MAX_BYTES . ' bytes', 101);
      }

      return $file_service->writeData(
        $response->getBody()->getContents(),
        'public://urlicon/' . str_replace('.', '-', $host) . '.ico',
        FileSystemInterface::EXISTS_REPLACE
      );
    }
    catch (\Exception $e) {
      \Drupal::logger('urlicon')->error('@code: @message', [
        '@code' => $e->getCode(),
        '@message' => $e->getMessage(),
      ]);
      return FALSE;
    }
  }

}
