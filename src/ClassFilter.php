<?php

namespace Drupal\urlicon;

use Drupal\Component\Utility\Html;

/**
 * The class filter.
 */
class ClassFilter extends BaseFilter {

  /**
   * {@inheritdoc}
   */
  protected static function callback(array $matches):string {
    $host = Html::escape(str_replace('.', '-', parse_url($matches[1], PHP_URL_HOST)));
    if (stristr($matches[0], 'class')) {
      $matches[0] = str_replace(
        'class="',
        'class="uc-' . $host . ' ',
        $matches[0]
      );
    }
    else {
      $matches[0] = str_replace(
        '">',
        '" class="urlicon urlicon-' . $host . '">',
        $matches[0]
      );
    }
    return (string) $matches[0];
  }

}
