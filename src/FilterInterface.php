<?php

namespace Drupal\urlicon;

/**
 * The filter interface.
 */
interface FilterInterface {

  /**
   * Filter a string.
   *
   * @param string $string
   *   The string to filter.
   *
   * @return string
   *   The filtered string.
   */
  public static function filter(string $string):string;

}
