<?php

namespace Drupal\urlicon\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\urlicon\ClassFilter;
use Drupal\urlicon\FaviconFilter;
use Drupal\urlicon\IconFilter;

/**
 * Provides a 'Urlicon' filter.
 *
 * @Filter(
 *   id = "urlicon_urlicon",
 *   title = @Translation("URL Icon filter"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   settings = {
 *     "type" = "favicon",
 *   },
 *   weight = -10
 * )
 */
class Urlicon extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'type' => 'favicon',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Filter URLs'),
      '#options' => [
        'favicon' => $this->t('Find all external URLs and append the according favicon (if available)'),
        'icon' => $this->t('Find all external URLs and append an external link icon'),
        'class' => $this->t('Find all external URLs and add a CSS class only (theme it as you like)'),
      ],
      '#default_value' => $this->settings['type'],
      '#description' => $this->t('Choose what to add to a link in the markup.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    switch ($this->settings['type']) {
      case 'class':
        return new FilterProcessResult(
          ClassFilter::filter($text)
        );

      case 'icon':
        return new FilterProcessResult(
          IconFilter::filter($text)
        );

      case 'favicon':
        return new FilterProcessResult(
          FaviconFilter::filter($text)
        );

    }
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Some filter tips here.');
  }

}
