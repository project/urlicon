<?php

namespace Drupal\urlicon;

use Drupal\Component\Utility\Html;
use Drupal\Core\Template\Attribute;

/**
 * The icon filter.
 */
class IconFilter extends BaseFilter {

  /**
   * {@inheritdoc}
   */
  protected static function callback(array $matches):string {
    /** @var \Drupal\Core\File\FileSystem $file_system */
    $file_system = \Drupal::service('file_system');
    if (!file_exists('public://urlicon/')) {
      $file_system->mkdir('public://urlicon/');
    }

    $favicon = \Drupal::service('extension.list.module')->getPath('urlicon') . '/assets/favicon.ico';
    $host = Html::escape(str_replace('.', '-', parse_url($matches[1], PHP_URL_HOST)));

    $element = [
      '#theme' => 'urlicon',
      '#text' => $matches[2],
      '#favicon' => \Drupal::service('file_url_generator')
        ->generateString($favicon),
      '#path' => $matches[1],
      '#attributes' => new Attribute([
        'class' => [
          'urlicon',
          'urlicon-' . $host,
        ],
      ]),
    ];
    $link = \Drupal::service('renderer')
      ->renderPlain($element);
    return trim($link);
  }

}
